package main

import (
	"fmt"
	kiai "gitlab.com/tapir/kiai/client"
	"math/rand"
	"sort"
	"time"
)

// Directions type
type Directions struct {
	ints  []int
	probs map[int]int
}

func NewDirections() Directions         { return Directions{[]int{0, 1, 2, 3, 4, 5, 6, 7}, make(map[int]int)} }
func (d Directions) Len() int           { return len(d.ints) }
func (d Directions) Swap(i, j int)      { d.ints[i], d.ints[j] = d.ints[j], d.ints[i] }
func (d Directions) Less(i, j int) bool { return d.probs[d.ints[i]] > d.probs[d.ints[j]] }

// States
const (
	ThreatState int = iota
	ExplorationState
	EndTurnState
)

const (
	West  int = -2
	North int = -1
	South int = 1
	East  int = 2
)

// Global game data
var (
	c                kiai.GameConfig
	t                *kiai.TurnStatus
	mainDirections   map[int][]int
	cornerDirections map[int][]int
	generalDirection int
	pl               int
)

func main() {
	c = kiai.Connect("localhost:4000", "Coshibo", 0, 1)
	defer kiai.Disconnect()

	rand.Seed(time.Now().UnixNano())

	// Set initial directions
	generalDirection = South

	mainDirections = make(map[int][]int)
	mainDirections[West] = []int{kiai.NorthWest, kiai.West, kiai.SouthWest}
	mainDirections[North] = []int{kiai.NorthWest, kiai.North, kiai.NorthEast}
	mainDirections[South] = []int{kiai.SouthWest, kiai.South, kiai.SouthEast}
	mainDirections[East] = []int{kiai.NorthEast, kiai.East, kiai.SouthEast}

	cornerDirections = make(map[int][]int)
	cornerDirections[West] = []int{kiai.NorthWest, kiai.West}
	cornerDirections[North] = []int{kiai.North, kiai.NorthEast}
	cornerDirections[South] = []int{kiai.SouthWest, kiai.South}
	cornerDirections[East] = []int{kiai.East, kiai.SouthEast}

	// Turn loop
	for {
		t = kiai.WaitForTurn()
		pl = t.PlayersLeft
		fmt.Printf("Turn loop set initial general Direction: %v\n", generalDirection)

		// Initial state deciding mechanism
		state := ExplorationState
		if checkThreat() {
			fmt.Printf("Turn loop switched state to: ThreatState\n")
			state = ThreatState
		} else {
			fmt.Printf("Turn loop switched state to: ExplorationState\n")
		}

		// State machine
		for {
			if state == ThreatState {
				state = Threat()
			} else if state == ExplorationState {
				state = Exploration()
			} else {
				fmt.Printf("Ending turn.\n\n")
				break
			}
		}

		kiai.EndTurn()
	}
}

// Check if there is any enemy around
func checkThreat() bool {
	for _, v := range t.Radar {
		if v > 1 {
			return true
		}
	}
	return false
}

// Sort 'most probable hit' firing directions
func calcFiringDirections() []int {
	d := NewDirections()

	i := 0
	for px, py := t.X-1, t.Y-1; px >= 0 && py >= 0; px, py = px-1, py-1 {
		i++
	}
	d.probs[kiai.NorthWest] = i

	i = 0
	for py := t.Y - 1; py >= 0; py-- {
		i++
	}
	d.probs[kiai.North] = i

	i = 0
	for px, py := t.X+1, t.Y-1; px < c.Width && py >= 0; px, py = px+1, py-1 {
		i++
	}
	d.probs[kiai.NorthEast] = i

	i = 0
	for px := t.X + 1; px < c.Width; px++ {
		i++
	}
	d.probs[kiai.East] = i

	i = 0
	for px, py := t.X+1, t.Y+1; px < c.Width && py < c.Height; px, py = px+1, py+1 {
		i++
	}
	d.probs[kiai.SouthEast] = i

	i = 0
	for py := t.Y + 1; py < c.Height; py++ {
		i++
	}
	d.probs[kiai.South] = i

	i = 0
	for px, py := t.X-1, t.Y+1; px >= 0 && py < c.Height; px, py = px-1, py+1 {
		i++
	}
	d.probs[kiai.SouthWest] = i

	i = 0
	for px := t.X - 1; px >= 0; px-- {
		i++
	}
	d.probs[kiai.West] = i

	sort.Sort(d)
	return d.ints
}

func Exploration() int {
	for {
		// Main logic
		if checkThreat() {
			fmt.Printf("[Exploration] Switching state to: ThreatState\n")
			return ThreatState
		} else {
			// End strategy:
			// Best strategy if we don't have enough moves and no fires is to
			// stay still so that we don't come across with somebody and get stuck
			// Best strategy if we don't have enough moves but some fires left
			// that are not enough to kill a healthy enemy is to fire to most probable dir
			if t.MovesLeft < 2 {
				fmt.Printf("[Exploration] End strategy activated.\n")
				dirs := calcFiringDirections()
				for i := 0; t.FiresLeft > 0; i++ {
					t.Fire(dirs[i%8])
					fmt.Printf("[Exploration] Shots fired. Fires Left: %v\n", t.FiresLeft)
				}
				fmt.Printf("[Exploration] Switching state to: EndTurnState\n")
				return EndTurnState
			} else {
				// Main move pattern for exploration
				// Check for walls and change mainDirection
				dir := mainDirections[generalDirection][rand.Intn(3)]

				if t.Radar[kiai.North] == 1 {
					generalDirection = East
					dir = cornerDirections[generalDirection][rand.Intn(2)]
				}

				if t.Radar[kiai.East] == 1 {
					generalDirection = South
					dir = cornerDirections[generalDirection][rand.Intn(2)]
				}

				if t.Radar[kiai.South] == 1 {
					generalDirection = West
					dir = cornerDirections[generalDirection][rand.Intn(2)]
				}

				if t.Radar[kiai.West] == 1 {
					generalDirection = North
					dir = cornerDirections[generalDirection][rand.Intn(2)]
				}

				if t.Radar[kiai.West] == 1 && t.Radar[kiai.North] == 1 {
					generalDirection = East
					dir = cornerDirections[generalDirection][rand.Intn(2)]
				}

				t.Move(dir)
				fmt.Printf("[Exploration] Moving for exploration. Moves Left: %v\n", t.MovesLeft)
			}
		}
	}
}

func Threat() int {
	var attackDirection int

	// Find the first enemy
	for i, v := range t.Radar {
		if v > 1 {
			attackDirection = i
		}
	}

	if t.MovesLeft > 0 {
		// Reposition to the opposite direction
		// so that we are out of enemies radar range
		t.Move((attackDirection + 4) % 8)
		fmt.Printf("[Threat] Moved for attack repositioning. Moves Left: %v\n", t.MovesLeft)
	}

	for t.FiresLeft > 0 {
		t.Fire(attackDirection)
		fmt.Printf("[Threat] Shots fired. Fires Left: %v\n", t.FiresLeft)

		// Check if enemy is dead and if so restart thread state
		// if there is still a threat otherwise threat is eliminated.
		// Switch to exploration mode.
		if t.PlayersLeft < pl {
			fmt.Printf("[Threat] Threat eliminated.\n")
			pl = t.PlayersLeft
			if checkThreat() {
				fmt.Printf("[Threat] There is another threat. Re-switching state to: ThreatState\n")
				return ThreatState
			} else {
				fmt.Printf("[Threat] Switching state to: ExplorationState\n")
				return ExplorationState
			}
		}
	}

	// Flee
	fmt.Printf("[Threat] Fleeing. Changed general direction to reverse.\n")
	generalDirection = -generalDirection

	fmt.Printf("[Threat] Switching state to: ExplorationState\n")
	return ExplorationState
}
